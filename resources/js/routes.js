import Home from './components/Home.vue';
import Counter from './components/Counter.vue';

export const routes = [
    { path: '/vue/hello', component: Home, name: 'Home' },
    { path: '/vue/counter', component: Counter, name: 'Counting' }
];