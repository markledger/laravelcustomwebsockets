import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import { routes } from './routes';
import store from './store';
import App from './app.vue';

Vue.use(Vuex);
Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes
});

new Vue({
  el: '#vue-app',
  store,
  router,
  template: '<App/>',
  components: { App }
});