import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    count: 66,
    chimp: "smell chimp",
    title: "My custom counter",
    links: [
    	'https://www.bbc.co.uk/sport',
    	'https://www.bbc.co.uk/news'
    ]
  },
  mutations: {
  	INCREMENT_COUNTER : (state, num )=> {
      state.count = parseInt(state.count) + parseInt(num);
    }
  },
  actions: {
    incrementCounter ({commit}) {                       // Add this
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            commit('INCREMENT_COUNTER', 77)
            resolve()
          }, 100)
        })
      }
  },
  getters:{
  	countLinks: state =>{
  		return state.links.length;
  	},
    linkProtocols: state => {
      return state.links.map(link => link.substr(0, link.indexOf(':')));
    }
  }
});

export default store;
