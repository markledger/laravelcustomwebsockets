<!DOCTYPE html>
<html>
<head>
	<title>CSV Sockets</title>
</head>
<body>
<h1>Upload your file</h1>

<form action="{{route('upload.post')}}" method="post" enctype="multipart/form-data">
    
    {{csrf_field()}}
    <input type="file" name="csv" id="csv">
    <button type="submit" name="submit">Upload</button>
</form>


<div id="socket-response">
</div>
</body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>


<script>

let form = document.querySelector('form');                               
var submit = form.querySelector('button');

submit.addEventListener('click', function(event){
    event.preventDefault();
    let csv = document.getElementById("csv").files[0];  // file from input
    var xhr = new XMLHttpRequest();
    let formData = new FormData();
    formData.append("csv", csv); 
    formData.append("_token", form.querySelector('[name=_token').value); 
    xhr.open("POST", form.action, true);
    xhr.send(formData);
})

var socket = io.connect('http://localhost:3001');

socket.on('connect', function () {
	var updates = document.getElementById('socket-response');
    socket.on('broadcast-user-{{ Auth::user()->id}}', function (data) {
        console.log(data);
        // socket.emit("broadcast", data);
        updates.append(data.text);
    });

    socket.on('disconnect', function () {
        console.log('disconnected');
    });
});
</script>