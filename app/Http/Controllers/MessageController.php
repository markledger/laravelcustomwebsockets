<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class MessageController extends Controller
{

   public function index(Request $request, $id = null)
   {  
      if(!is_null($id)){
   	  $data = Message::find($id)->first();
        return response(['data' => $data], 200);
      }
   	
      return response(['data' => Message::all()], 200);
   	  
   }
	
}
