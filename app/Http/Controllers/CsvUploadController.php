<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;
use Maatwebsite\Excel\Facades\Excel;

class CsvUploadController extends Controller
{

	public function __construct()
	{
	    $this->middleware('auth');
	}

    public function upload(Request $request){
		$client = new Client(new Version2X('http://localhost:3001', [
			'headers' => [
		        'cookie: laravel_session=' . $_COOKIE['laravel_session']
    		]
		]));

		$client->initialize();

		$handle = fopen($request->file('csv'), "r");

		while ($csvLine = fgetcsv($handle)) {
			sleep(1);
			$client->emit('broadcast-upload', [
				'type' => 'notification', 'text' => $csvLine
				]);
		}
		$client->close();
    }


}
