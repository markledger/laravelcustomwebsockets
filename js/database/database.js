var mysql      = require('mysql');
var database = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'root',
  database : 'manage'
});
 
database.connect();
 
 module.exports = database;