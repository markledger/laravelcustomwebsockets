var cookie      = require('cookie');
    MCrypt      = require('mcrypt').MCrypt;
    env         = require('../config/env');
    sessionManager	= require('../database/sessionManager');

module.exports = authenticateUser;

function authenticateUser(socket, next){
	if(!socket.handshake.headers.cookie){
		return next(new Error('Header contained no cookies'));
	}
	
	let cookies = cookie.parse(socket.handshake.headers.cookie);	

	if(!cookies.laravel_session){
		return next(new Error('Header contained no framework cookie'));
	}

	let laravelCookie = JSON.parse(Buffer.from(cookies.laravel_session, 'base64'));
	let iv    =  Buffer.from(laravelCookie.iv, 'base64');
	let value =  Buffer.from(laravelCookie.value, 'base64');
	let rijCbc = new MCrypt('rijndael-128', 'cbc');
		rijCbc.open(env.secretKey, iv);
	let decrypted = rijCbc.decrypt(value).toString();

	let length = decrypted.length - 1;
	let pad = decrypted.charAt(length).charCodeAt(0);
	let sessionID = decrypted.substr(0, decrypted.length - pad)

	sessionManager.getUserFromSessionID(sessionID, function(err, user) {
        if (!err && user) {
        	socket.user = user; 
        } else {
        	return next(new Error('Authentication error'));
        };
    });

	return next();

}

