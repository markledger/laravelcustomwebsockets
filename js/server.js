var socket      = require('socket.io'),
    // https       = require('https'),
    http        = require('http'),
    logger      = require('winston'),
    port        = 3001;
    authenticateUser = require('./middleware/authenticateUser');

// Logger config
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, { colorize: true, timestamp: true });
logger.info('SocketIO > listening on port ' + port);

var http_server  = http.createServer();
var io = socket(http_server);
io.use(authenticateUser);

//io.sockets.on('connection', function(socket){
io.on('connection', function(socket){
	console.log("Initial Connection");	
	socket.on('broadcast-upload', function(data){
		io.use(authenticateUser).emit('broadcast-user-'+socket.user.id, data);
	});
});

http_server.listen(port, function() {
    console.log('Node.js server listening on port ' + port);
});



 	



// emitNewOrder(http_server);	