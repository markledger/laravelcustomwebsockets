<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\User;

$factory->define(App\Article::class, function (Faker $faker) {
    return [
    	'author_id' => User::all()->random()->id,
        'title' => $faker->sentence(9, true),
        'body' => $faker->paragraph(10)
    ];
});
